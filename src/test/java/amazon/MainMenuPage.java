package amazon;


import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.$x;

public class MainMenuPage {

    public MainMenuPage clickComputers(){
        $x("//div[text()=\"Computers\"]").click();
        return this;
    }
    public ComputersMenuPage waitMenuComputersVisible(){
        $x("//div[@class=\"hmenu-item hmenu-title \"][text()=\"computers\"]").shouldBe(visible);
        return new ComputersMenuPage();
    }
}
