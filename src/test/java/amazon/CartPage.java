package amazon;


import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.$x;

public class CartPage {
public CartPage assertTitleEquals(String expectedName ){
    $x("//div[@data-minquantity=\"1\"][text()]").shouldHave(visible);
    $x("//div[@data-minquantity=\"1\"][text()]").getText().contains(expectedName);
        return this;
        }

    public String getTitleLaptop (){
        $x("//div[@data-minquantity=\"1\"][text()]").shouldHave(visible);
        return $x("//div[@data-minquantity=\"1\"][text()]").getText();

    }
}
