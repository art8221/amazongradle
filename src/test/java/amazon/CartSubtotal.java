package amazon;


import static com.codeborne.selenide.Condition.text;
import static com.codeborne.selenide.Selenide.$x;

public class CartSubtotal  {
    public CartPage clickCartButtom(){
        if ($x("//*[@class=\"a-size-small a-color-base\"][text()='Subtotal']").isDisplayed())
        {
            $x("//div[@id=\"nav-cart-count-container\"]").click();
        }
        else if ($x("//*[@class=\"a-size-small a-color-base\"][text()='Subtotal']").isEnabled()){
            $x("//span[@class=\"a-button a-button-base attach-button-large attach-cart-button\"]").click();
        }
        else if($x("//*[@id=\"sc-active-cart\"]/div/div/div/h1").shouldBe(text("\n" + "Shopping Cart")).isDisplayed())
        {
            $x("//div[@data-asin=\"B08YKFTZH6\"]").click();
        }

        return new CartPage();
    }
}
