package amazon;


import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.$x;

public class LaptopComputers {
    public LaptopComputers clickSearchResultFirst(){
        $x("//div[@data-index=\"1\"]//span[@class=\"a-size-medium a-color-base a-text-normal\"]").click();
        return this;
    }
    public LaptopsProductPage waitResultFirst(){
        $x("//span[@id=\"productTitle\"]").shouldHave(visible);
        return new LaptopsProductPage();
    }

}
