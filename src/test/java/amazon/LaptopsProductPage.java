package amazon;


import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.$x;

public class LaptopsProductPage {
    public LaptopsProductPage clickLaptopsResult() {
        $x("//div[@cel_widget_id=\"MAIN-SEARCH_RESULTS-1\"]").click();
        $x("//span[@id=\"productTitle\"]").shouldBe(visible);
        return new LaptopsProductPage();
    }

    /*public LaptopsProductPage setProductTitle() {
        this.titleProduct = productTitle.getText().replaceAll(" ", "").substring(0, 30);
        return this;
    }*/


    public LaptopsProductPage changeDeliver() {
        $x("//div[@id=\"contextualIngressPtLabel_deliveryShortLine\"]").shouldHave(visible);
        $x("//div[@id=\"contextualIngressPtLabel_deliveryShortLine\"]").click();
        $x("//h4[@class=\"a-popover-header-content\"]").shouldBe(visible);
        $x("//span[@class=\"a-button-dropdown a-button a-button-span12\"]").click();
        $x("//a[@id=\"GLUXCountryList_1\"][text()='Canada']").shouldBe(visible);
        $x("//a[@id=\"GLUXCountryList_1\"][text()='Canada']").click();
        $x("//button[@name=\"glowDoneButton\"]").shouldBe(visible);
        $x("//button[@name=\"glowDoneButton\"]").click();
        $x("//div[@id=\"contextualIngressPtLabel_deliveryShortLine\"]//span[text()=\"Canada\"]").shouldBe(visible);
        return this;
    }

    public CartSubtotal addToCart() {
        $x("//input[@id=\"add-to-cart-button\"]").click();
        $x("//span[@id=\"nav-cart-count\"][text()=\"1\"]").shouldBe(visible);
        return new CartSubtotal();
    }

}