package amazon;


import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.$x;

public class ComputersTabletsPage {
    public ComputersTabletsPage clickLaptops(){
        $x("//span[text()=\"Laptops\"]").click();
        return this;
    }
    public LaptopsPage waitLaptopsResultVisible(){
        $x("//span[@class=\"a-size-base a-color-base a-text-bold\"][text()=\"Laptops\"]").shouldBe(visible);
        return new LaptopsPage();
    }
}
