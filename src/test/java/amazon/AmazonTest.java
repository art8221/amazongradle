package amazon;


import core.BaseTest;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import static com.codeborne.selenide.Selenide.open;

public class AmazonTest extends BaseTest {
    private final static String URL="https://www.amazon.com";
    private static String laptopName ="Lenovo IdeaPad Flex 5i - 2022 - Chromebook 2-in-1 Laptop - Chrome OS - 13.3\" " +
            "FHD Touch Display - 8GB Memory - 128GB Storage - Intel Core i3 11th Gen - Abyss Blue\n";

    @Tag("UI")
    @Test
    public void amazonTestAddToCartLaptop(){
         open(URL, MainPage.class)
                .clickMainManu()
                .waitHmenuItemVisible()
                .clickComputers()
                .waitMenuComputersVisible()
                .clickComputersTablets()
                .waitComputersTabletsItemVisible()
                .clickLaptops()
                .waitLaptopsResultVisible()
                .searchLaptop(laptopName)
                .clickButtom()
                .waitSeachResultVisible()
                .clickSearchResultFirst()
                .waitResultFirst()
                .changeDeliver()
                .addToCart()
                .clickCartButtom()
                .assertTitleEquals(laptopName)
        ;

    }




}
